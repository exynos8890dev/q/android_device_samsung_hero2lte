PRODUCT_MAKEFILES := \
	$(LOCAL_DIR)/aosp_hero2lte.mk \
	$(LOCAL_DIR)/lineage_hero2lte.mk \
	$(LOCAL_DIR)/havoc_hero2lte.mk \
	$(LOCAL_DIR)/potato_hero2lte.mk \
	$(LOCAL_DIR)/bootleg_hero2lte.mk \
	$(LOCAL_DIR)/stag_hero2lte.mk \

COMMON_LUNCH_CHOICES := \
    lineage_hero2lte-userdebug \
	aosp_hero2lte-userdebug \
	havoc_hero2lte-userdebug \
	potato_hero2lte-userdebug \
	bootleg_hero2lte-userdebug \
	stag_hero2lte-userdebug \

